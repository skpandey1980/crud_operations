package com.example.demo;


import com.example.demo.dto.CommentDto;
import com.example.demo.entity.Comment;
import com.example.demo.repositories.CommentRepository;
import com.example.demo.services.CommentCRUDService;
import com.example.demo.services.CommentFilterService;
import com.example.demo.services.CommentIndexService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = CrudApplication.class)
public class HeavyLoadTest {
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private CommentCRUDService cts;

    @Autowired
    private CommentFilterService filterService;

    @Autowired
    private CommentIndexService indexService;


    int l1Size = 500;
    int l2Depth =100;
    int depthStep = 20;
    int maxDepth = 90;

    int R = 2;//The total number of comments = R+l1Size+R*l1Size*l2Depth; OOM for R>5;

    List<Comment> r;
    List<List<Comment>> L1list;

    List<List<List<Comment>>> LBig;

    @BeforeEach
    public void setup() {
        commentRepository.deleteAll();
        commentRepository.flush();
        r= new ArrayList<>();
        Comment tR;
        for(int i=0;i<R;i++){
            tR=buildComment("r_"+i, 1);

            persistEntity(cts, null, tR);
            r.add(tR);
        }

        listProducer flb = new FirstLevelBuilder();
        L1list = new ArrayList<>();
        for(int i=0;i<R;i++){
            L1list.add(flb.produceList(r.get(i),  l1Size,"l"+i+" "));
        }

        listProducer ptb = new PinTreeBuilder();

        LBig = new ArrayList<>();

        for(int j=0;j<R;j++){
            List<List<Comment>> tempL = new ArrayList<>();
            for(int i=0;i<L1list.get(j).size();i++){
                tempL.add(ptb.produceList(L1list.get(j).get(i), l2Depth, "r "+j+" "+i));
            }
            LBig.add(tempL);
        }


    }

    @AfterEach
    public void closeUp(){

        commentRepository.deleteAll();
        commentRepository.flush();

    }


    @Test
    public void findAllChildCommentsOfGivenSingleRootComment(){

        Comment el1=LBig.get(1).get(l1Size/2).get(51);
        String path = el1.getPathString();
        Collection<Comment> commentCollection = cts.findCommentsByPathWithParent(path);
        CommentDto result = filterService.filterComments(commentCollection,el1);

        assertEquals(filterService.giveTotalSize(result), 1);



        int numberOfRuns=1;

        long start = System.currentTimeMillis();
        for(int j=0;j<numberOfRuns;j++) {
            for (Comment el : L1list.get(0)) {
                testSearchLike(el, -2);
            }
        }
        long end = System.currentTimeMillis();
        float diff = (end - start);
        System.out.println("runtime like for depth:"+2+", the diff is "+diff);

        start = System.currentTimeMillis();
        for(int j=0;j<numberOfRuns;j++) {
            for (Comment el :  L1list.get(0)) {
                testSearchIndex(el, -2);
            }
        }
        end = System.currentTimeMillis();
        diff = (end - start);
        System.out.println("runtime index for depth:"+2+", the diff is "+diff);

        for(int depth =10;depth<=maxDepth;depth=depth+depthStep) {

            start = System.currentTimeMillis();
            for(int j=0;j<numberOfRuns;j++) {

                for (List<Comment> el : LBig.get(0)) {
                    testSearchLike(el.get(depth), depth);
                }
            }
            end = System.currentTimeMillis();
            diff = (end - start);
            System.out.println("runtime like for depth:" + depth + ", the diff is " + diff);

            start = System.currentTimeMillis();
            for(int j=0;j<numberOfRuns;j++) {
                for (List<Comment> el : LBig.get(0)) {
                    testSearchIndex(el.get(depth), depth);
                }
            }
            end = System.currentTimeMillis();
            diff = (end - start);
            System.out.println("runtime index for depth:" + depth + ", the diff is " + diff);
        }
    }

    void persistEntity(CommentCRUDService cts, Comment parent, Comment child){
        if(parent!=null || child.getDepth()>1 ) {
            child.setParent(parent.getId());
            cts.saveComment(child, parent.getPathString());
        }
        else{
            cts.saveRoot(child);
        }
    }


    public interface listProducer{
        public List<Comment> produceList(Comment parent, int size, String content);
    }



    public class FirstLevelBuilder implements listProducer{
        public List<Comment> produceList(Comment parent, int size, String content){
            List<Comment> l1 = new ArrayList<>();
            Comment temp;
            for(int i=0;i<size;i++){
                temp = buildComment(content+" "+i, 2);

                persistEntity(cts, parent, temp);
                l1.add(temp);
            }
            return l1;
        }
    }
    public class PinTreeBuilder implements listProducer{
        public  List<Comment> produceList(Comment parent, int depth, String content){
            int intDepth=3;
            List<Comment> l1 = new ArrayList<>();
            Comment temp;
            temp = buildComment(content+" "+0, intDepth);
            persistEntity(cts, parent, temp);
            l1.add(temp);
            temp = buildComment(content+" "+1, intDepth);
            persistEntity(cts, parent, temp);
            l1.add(temp);
            intDepth++;

            for(int i=2;i<depth;i=i+2){
                temp = buildComment(content+" "+i, intDepth);
                persistEntity(cts, l1.get(i-2), temp);
                l1.add(temp);
                temp = buildComment(content+" "+i+1, intDepth);
                persistEntity(cts, l1.get(i-2), temp);
                l1.add(temp);
                intDepth++;
            }
            return l1;
        }
    }

    public Comment buildComment(String content, int depth){
        Comment temp;
        temp= Comment.builder()
                .content(content)
                .author(4)
                .timestamp(null)
                .build();
        temp.setDepth(depth);
        return temp;
    }

    public void testSearchIndex(Comment com, long ind){
        String path = com.getPathString();
        Collection<Comment> commentCollection = cts.findCommentsByPathWithParent(path);
        CommentDto result = filterService.filterComments(commentCollection,com);
        assertEquals(filterService.giveTotalSize(result), l2Depth-ind-1L);

    }
    public void testSearchLike(Comment com, long ind){
        String path = com.getPathString();
        Collection<Comment> commentCollection = commentRepository.findAllChildCommentsOfPath(path);
        CommentDto result = filterService.filterComments(commentCollection,com);
        assertEquals(filterService.giveTotalSize(result), l2Depth-ind -1L);

    }

}
