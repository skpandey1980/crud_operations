package com.example.demo.repositories;


import com.example.demo.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    @Query( "SELECT c FROM Comment c WHERE c.index >:from AND c.index <:to" )
    Collection<Comment> findAllChildrenByIndex(@Param("from") BigDecimal from, @Param("to") BigDecimal to);

    @Query( "SELECT c FROM Comment c WHERE c.pathString LIKE :path%" )
    Collection<Comment> findAllChildCommentsOfPath(@Param("path") String path);

    @Query( "SELECT c FROM Comment c WHERE c.depth=1 OR c.pathString=null " )
    Collection<Comment> findAllRootComments();

    @Query( "SELECT c FROM Comment c WHERE c.depth=1 AND c.author=:uname " )
    Collection<Comment> findAllRootCommentsByUser(String uname);
}