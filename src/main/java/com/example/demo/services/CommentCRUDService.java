package com.example.demo.services;

import com.example.demo.dto.CommentDto;
import com.example.demo.entity.Comment;

import java.util.Collection;
import java.util.Optional;

public interface CommentCRUDService {
    public void saveComment(Comment comment, String parentPath);
    public void saveRoot(Comment comment);
    public Collection<Comment> findCommentsByPathWithParent(String path);
    public Collection<Comment> findAllRootComments();
    public Optional<Comment> findRootCommentById(long id);
    public Comment updateRoot(Comment c);
    public CommentDto findCommentDto(Comment parent);
    public void deleteComment(Comment parent);
    public Comment updateSubRoot(Comment c);

}
