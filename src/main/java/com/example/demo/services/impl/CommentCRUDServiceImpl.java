package com.example.demo.services.impl;

import com.example.demo.dto.CommentDto;
import com.example.demo.entity.Comment;
import com.example.demo.repositories.CommentRepository;
import com.example.demo.services.CommentCRUDService;
import com.example.demo.services.CommentFilterService;
import com.example.demo.services.CommentIndexService;
import com.example.demo.services.ComputationConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.web.context.annotation.SessionScope;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;

@Service
@SessionScope
public class CommentCRUDServiceImpl implements CommentCRUDService {

    @Value("${comment.crud.scale}")
    private int SCALE;

    private static MathContext mc;
    private static BigDecimal eps;
    private static BigDecimal one;
    private static BigDecimal half;

    @Autowired
    CommentRepository comRepo;
    
    @Autowired
    CommentIndexService indexService;

    @Autowired
    CommentFilterService filterService;

    @Autowired
    public CommentCRUDServiceImpl(ComputationConfigService ccs){

        this.mc = ccs.giveMathContext();
        this.one = ccs.giveOne();
        this.half = ccs.giveHalf();
        this.eps = ccs.giveDbEps();

    }
    //CommentExt comes with its parent's path in the pathString!
    //CommentExt comes with its depth in the depth field!
    //CommentExt comes in the transient state!
    @Override
    public void saveComment(Comment comment, String parentPathString) {
        comment.setPathString(parentPathString);
        comment = indexService.setPathRnd(comment);
        comment = indexService.setDepthOutOfPath(comment);
        comment = indexService.computeIndex(comment);
        comRepo.saveAndFlush(comment);
    }

    @Override
    @Transactional
    public void saveRoot(Comment comment) {
        comment = comRepo.save(comment);
        comment = indexService.setPathRnd(comment);
        comment = indexService.setDepthOutOfPath(comment);
        comment = indexService.computeIndex(comment);

    }


    @Override
    public Collection<Comment> findCommentsByPathWithParent(String path) {
        if(path==null) {
            return new ArrayList<>();
        }
        Comment c = new Comment();
        c.setPathString(path);
        c = indexService.computeIndex(c);
        BigDecimal to = indexService.computeNeighbourBra(c);
        BigDecimal from = indexService.computeNeighbourKet(c);
        BigDecimal diff = from.subtract(to);
        if(from.compareTo(to)==1) {
            return comRepo.findAllChildrenByIndex(to.subtract(eps,mc), from.add(eps,mc));
        }
        else{
            return comRepo.findAllChildrenByIndex(from.subtract(eps,mc),to.add(eps,mc) );
        }
    }

    @Override
    public CommentDto findCommentDto(Comment parent){
        Collection<Comment> comments = findCommentsByPathWithParent(parent.getPathString());
        return filterService.filterComments(comments,parent);
    }
    @Override
    @Transactional
    public void deleteComment(Comment parent){
        Collection<Comment> comments = findCommentsByPathWithParent(parent.getPathString());
        comments = filterService.filterCommentsForDelete(comments,parent);
        comRepo.deleteAll(comments);
    }
    @Override
    public Comment updateSubRoot(Comment c) {
        return comRepo.saveAndFlush(c);
    }


    @Override
    @Cacheable(value = "rootComments", key = "'all'")
    public Collection<Comment> findAllRootComments(){
        return comRepo.findAllRootComments();
    }

    @Override
    @Cacheable(value = "rootComments",key = "#id")
    public Optional<Comment> findRootCommentById(long id) {
        return comRepo.findById(id);
    }

    @Override
    @CachePut(value = "rootComments", key = "#c.id")
    public Comment updateRoot(Comment c) {
        return comRepo.saveAndFlush(c);
    }
}
